#!/usr/bin/python2
import numpy as np
import cv2

from sys import argv
from os import path

from time import time
def fps():
    global a, c
    if 'a' in globals() and 'c' in globals() and time()-a < 1:
        c+=1
    else:
        if 'c' in globals(): print ("fps: %d " % c)
        c=0
        a=time()

cam = False
cap = None
picture=None

windows_name1='blur effects'

def usage():
    print("usage: %s <-c [CAM_NUM] | -p [PICTURE_FILE]>" % argv[0])

class Rectangle():
    x=0
    y=0
    h=0
    w=0
    c=()
    def __init__(self,x,y,b,h):
        self.x = int(x)
        self.y = int(y)
        self.b = int(b)
        self.h = int(h)
        self.c = ( int(bx+(bw/2)) , int(by+(bh/2)) )

    def distance(self,rect):
        return cv2.norm( (self.c[0], self.c[1]) , (rect.c[0],rect.c[1]) )
    def area(self):
        return self.h*self.b

if len(argv) is 3:
    ''' Parse args '''
    if str(argv[1]).strip() == "-c":    
        if argv[2].isdigit():
            ncam=int(argv[2])
            cam = True
        else:
            print("error parsing arguments: '%s' is not a digit" % argv[2])
            exit(1)
    elif str(argv[1]).strip() == "-p":    
        if path.exists(argv[2]):
            picture=argv[2]
            cam = False
        else:
            print("error parsing arguments: '%s' is not a file" % argv[2] )
            exit(1)
    else:
        usage()
        exit(1)
else:
    usage()
    exit(1)
  

if cam :
    ''' Inizialize webcam '''
    cap = cv2.VideoCapture(ncam)

def do_nothing(a):
    pass

''' Create commands windows '''
cv2.namedWindow( windows_name1, cv2.WINDOW_AUTOSIZE )
cv2.createTrackbar("blur", windows_name1 , 3, 3, do_nothing )
cv2.createTrackbar("blur val", windows_name1 , 7, 30, do_nothing )
cv2.createTrackbar("threshold", windows_name1 , 230, 255, do_nothing )
cv2.createTrackbar("FindContours mode", windows_name1,3,3, do_nothing )
cv2.createTrackbar("FindContours method", windows_name1,1,3, do_nothing )
cv2.createTrackbar("Min area", windows_name1,2500,5000, do_nothing )

while( True ):
    ''' Read image '''
    if cam :
        ''' Capture frame-by-frame '''
        ret, img = cap.read()
    else:
        ''' Read image file '''
        img =  cv2.imread(picture)
        if img is None:
            print("%: file not found")
            exit(1)

    ''' Read trackbar values '''
    BLUR_TYPE=cv2.getTrackbarPos("blur", windows_name1)
    BLUR_VAL=cv2.getTrackbarPos("blur val", windows_name1)
    THRESHOLD=cv2.getTrackbarPos("threshold", windows_name1)
    MIN_AREA=cv2.getTrackbarPos("Min area", windows_name1)
    # fix not valid values
    if BLUR_VAL % 2 == 0 :
        BLUR_VAL+=1
        cv2.setTrackbarPos("blur val", windows_name1,BLUR_VAL)

    ''' FindContours mode '''
    FINDCONTOURS_MODE=cv2.getTrackbarPos("FindContours mode", windows_name1)
    if FINDCONTOURS_MODE is 0:
        FINDCONTOURS_MODE = cv2.RETR_EXTERNAL
        findContours_mode_name="CV_RETR_EXTERNAL"
    elif FINDCONTOURS_MODE is 1:
        FINDCONTOURS_MODE = cv2.RETR_LIST
        findContours_mode_name="CV_RETR_LIST"
    elif FINDCONTOURS_MODE is 2:
        FINDCONTOURS_MODE = cv2.RETR_CCOMP
        findContours_mode_name="CV_RETR_CCOMP"
    elif FINDCONTOURS_MODE is 3:
        FINDCONTOURS_MODE = cv2.RETR_TREE
        findContours_mode_name="CV_RETR_TREE"
    else:
        print("not valid FindContours mode")
        exit(1)

    FINDCONTOURS_METHOD=cv2.getTrackbarPos("FindContours method", windows_name1)
    if FINDCONTOURS_METHOD is 0:
        FINDCONTOURS_METHOD=cv2.CHAIN_APPROX_NONE
        findContours_method_name="CV_CHAIN_APPROX_NONE"
    elif FINDCONTOURS_METHOD is 1:
        FINDCONTOURS_METHOD=cv2.CHAIN_APPROX_SIMPLE
        findContours_method_name="CV_CHAIN_APPROX_SIMPLE"
    elif FINDCONTOURS_METHOD is 2:
       FINDCONTOURS_METHOD=cv2.CHAIN_APPROX_TC89_KCOS
       findContours_method_name="CV_CHAIN_APPROX_TC89_KCOS"
    elif FINDCONTOURS_METHOD is 3:
       FINDCONTOURS_METHOD=cv2.CHAIN_APPROX_TC89_L1
       findContours_method_name="CV_CHAIN_APPROX_TC89_L1"
    else:
        print("not valid FindContours method")
        exit(1)

    ''' GRAY '''
    ''' Conversione immagine in scala di grigi '''
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ''' Blur '''
    if BLUR_TYPE is 0:
        blur = gray
        blur_method="None"
    elif BLUR_TYPE is 1:
        blur = cv2.blur(gray,(BLUR_VAL,BLUR_VAL))
        blur_method="Blur"
    elif BLUR_TYPE is 2:
        blur = cv2.GaussianBlur(gray,(BLUR_VAL,BLUR_VAL),0)
        blur_method="Gaussian Blur"
    elif BLUR_TYPE is 3:
        blur = cv2.medianBlur(gray,BLUR_VAL)
        blur_method="Median Blur"
    else:
        print("not valid blur type")
        exit(1)
    
    ''' THRESHOLD '''
    ''' Apply threshold for gray to binary conversion '''
    ret,thresh = cv2.threshold(blur,THRESHOLD,255,cv2.THRESH_BINARY)
    
    ''' Draw rect '''
    rects = []
    rets_vals = []
    edges = cv2.Canny(thresh,THRESHOLD,THRESHOLD*2)
    drawing = np.zeros(img.shape,np.uint8)
    contours,hierarchy = cv2.findContours(edges,FINDCONTOURS_MODE,FINDCONTOURS_METHOD)

    for cnt in contours:
        bx,by,bw,bh = cv2.boundingRect(cnt)
        if bw*bh>MIN_AREA:
            if (bx,by,bh,bw) not in rets_vals:
                rets_vals.append( (bx,by,bh,bw) )
                r = Rectangle(bx,by,bh,bw)
                rects.append( r )
                # Draw rectangle ( blue )
                cv2.rectangle(drawing,(bx,by),(bx+bw,by+bh),(255,0,0),2)
                # Draw center of rectangle
                cv2.circle(drawing,( int(bx+(bw/2)) , int(by+(bh/2)) ), 2,(0,255,255),2 )
                # Stamp rectangle area
                cv2.putText(drawing, "A: %d" % r.area(), ( int(bx+(bw/2)) , int(by+(bh/2)) ), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,255) )
    rets_vals = None
    def draw_dist(r1,r2):
        global drawing
        p1=(r1.c[0],r1.c[1])
        p2=(r2.c[0],r2.c[1])
        cv2.line(drawing,p1,p2,(255,255,255),1)
        mx=(p1[0]+p2[0])/2
        my=(p1[1]+p2[1])/2
        cv2.putText(drawing, "d: %d" % r1.distance(r2), (mx,my), cv2.FONT_HERSHEY_PLAIN, 1.0, (255,255,255) )
        return r1
    for i in range(len(rects)):
        reduce(draw_dist,rects[i:])

    cv2.putText(drawing, "Markers: %d" % len(rects), (20,20), cv2.FONT_HERSHEY_PLAIN, 1.0, (255,255,255) )

    ''' Display the resulting frames '''
    cv2.imshow("source",img)
    cv2.imshow("gray",gray)
    cv2.putText(blur, blur_method, (20,20), cv2.FONT_HERSHEY_PLAIN, 1.0, (255,255,255) )
    cv2.imshow("blur",blur)
    txt="mode %s method: %s" % (findContours_mode_name,findContours_method_name)
    cv2.putText(edges, txt, (20,20), cv2.FONT_HERSHEY_PLAIN, 1.0, (255,255,255) )
    cv2.imshow("edges",edges)
    cv2.imshow('threshold',thresh)
    cv2.imshow('drawed',drawing)

    ''' Terminare script when [Q] key is pressed '''
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    fps()

''' End '''
if cam :
    cap.release()
cv2.destroyAllWindows()