@echo off
:: Used to create a Python exe 

:: get rid of all the old files in the build folder
rd /S /Q build
rd /S /Q dist

:: copy necessary files
copy /Y /V C:\Anaconda\Lib\site-packages\win32\pywintypes27.dll  C:\Anaconda\Lib\site-packages\win32\lib\

copy /Y /V C:\Anaconda\Lib\site-packages\win32\pythoncom27.dll  C:\Anaconda\Lib\site-packages\win32\lib\


:: create the exe
python -OO setup.py py2exe

:: delete libs copied
del C:\Anaconda\Lib\site-packages\win32\lib\pywintypes27.dll
del C:\Anaconda\Lib\site-packages\win32\lib\pythoncom27.dll

:: pause so we can see the exit codes
pause "done...hit a key to exit"