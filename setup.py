import py2exe
import numpy
import sys
from distutils.core import Distribution,setup
from glob import glob

class WinDistribution(Distribution):
	dfiles = [("pywintypes27",
              glob(r'C:\Anaconda\Lib\site-packages\win32\pywintypes27.dll')), 
             ("pythoncom",
              glob(r'C:\Anaconda\Lib\site-packages\win32\pythoncom27.dll'))]

	py2exe_options= dict(
                      ascii=True,  
                      excludes=['_ssl',  
                                'pyreadline', 'difflib', 'doctest', 'locale', 
                                'optparse', 'pickle', 'calendar'],  
                      dll_excludes=['msvcr71.dll'], 
                      compressed=True,  
                      optimize=2
                      )
	def __init__(self, attrs):
		Distribution.__init__(self, attrs)
		self.com_server = []
		self.services = []
		self.windows = ['FindWhiteMarker.py']
		self.console = ['FindWhiteMarker.py']
		self.options = {'py2exe':  self.py2exe_options}
		self.data_files = self.dfiles
		self.zipfile = 'library.zip'
		self.name='FindWhiteMarker',
		self.version='1.0',
		self.description=' ',
		self.author='Andrea Cattaneo'
		



setup(distclass=WinDistribution)
#setup(distclass=WinDistribution,options={'py2exe': py2exe_options})
#setup(windows=['FindWhiteMarker.py'],options=py2exe_options)